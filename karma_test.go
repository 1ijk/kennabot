package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var karmaExamples = []struct {
	expect Karma
	result Karma
}{
	{Karma{"kennabot", "karma", 1}, Read("kennabot++ for karma")},
	{ Karma{"kennabot", "", More}, Read("kennabot++")},
	{Karma{"", "karma", 1}, Read("++ for karma")},
	{MoreKarma, Read("++")},
	{MoreKarma, Read(" ++")},
	{MoreKarma, Read("++ ")},
	{LessKarma, Read("--")},
	{LessKarma, Read(" --")},
	{LessKarma, Read("-- ")},
	{NoKarma, Read("+-")},
	{NoKarma, Read("-+ ")},
	{Karma{"kennabot", "karma", 1}, Read("@kennabot++ for karma")},
	{WarnKarma, Read("kennabot has 100000000 karma for being the best ever")}, // hit an infinite loop with this somehow
}

func TestRead(t *testing.T) {
	for _, subject := range karmaExamples {
		assert.Equal(t, subject.expect, subject.result)
	}
}
