package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var subject = Wheel{NoKarma}

var wheelExamples = []struct {
	expect Karma
	result Karma
}{
	{NoKarma, subject.Turn("nonsense")},
	{Karma{"kennabot", "", More}, subject.Turn("kennabot++")},
	{Karma{"kennabot", "karma", More}, subject.Turn("kennabot++ for karma")},
	{Karma{"kennabot", "karma", More}, subject.Turn("kennabot++")},
	{Karma{"kennabot", "karma", More}, subject.Turn("++")},
	{Karma{"kennabot", "karma", Less}, subject.Turn("--")},
	{Karma{"kennabot", "stuff", More}, subject.Turn("++ for stuff")},
	{Karma{"kenabuff", "stuff too", More}, subject.Turn("kenabuff++ for stuff too")},
	{Karma{"kenabuff", "stuff too", More}, subject.Turn("++")},
	{Hell.Eye, subject.Turn("kennabot has 100000000 karma for being the best ever")},
	{NoKarma, subject.Turn("some benign conversation")},
	{Karma{"kenabuff", "stuff too", More}, subject.Turn("++")},
}

func TestWheel_Turn(t *testing.T) {
	for _, example := range wheelExamples {
		assert.Equal(t, example.expect, example.result)
	}
}
