package main

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"time"
)

type Dharma struct {
	*Wheel
	*time.Ticker
	*redis.Client
}

func Start() Dharma {
	return Dharma{
		Wheel:  &Wheel{NoKarma},
		Ticker: time.NewTicker(5 * time.Second),
		Client: redis.NewClient(&redis.Options{
			Addr:     "localhost:6379",
			Password: "hahahahaha",
		}),
	}
}

func (d *Dharma) Consume(ctx context.Context, message string) (string, error) {
	karma := d.Wheel.Turn(message)
	if karma == Hell.Eye {
		return "", ErrOffendingKrishna
	}

	// infinite feedback loop without this
	if karma == NoKarma {
		return "", nil
	}

	return d.record(ctx)
}

func (d *Dharma) record(ctx context.Context) (string, error) {
	var judgement string
	points, err := d.HIncrBy(ctx, d.Eye.Who, "total", d.Eye.Val).Result()
	if err != nil {
		return "", err
	}

	judgement = fmt.Sprintf("%s has %d karma in total", d.Eye.Who, points)

	if d.Eye.Why != "" {
		points, err = d.HIncrBy(ctx, d.Eye.Who, d.Eye.Why, d.Eye.Val).Result()
		if err != nil {
			return "", err
		}
		judgement = fmt.Sprintf("%s has %d karma for %s", d.Eye.Who, points, d.Eye.Why)
	}

	return judgement, nil
}
