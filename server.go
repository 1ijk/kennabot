package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/slack-go/slack"
	"github.com/slack-go/slack/slackevents"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

var (
	kennabot = Start()
	api      = slack.New(os.Getenv("KENNABOT_TOKEN"))
	quote    = "you turned the wheel of the Law in the thousand-million-fold world,\n" +
		"the wheel that from the first has always been pure."
)

func main() {
	signingSecret := os.Getenv("SLACK_SIGNING_SECRET")

	http.HandleFunc("/events-endpoint", func(w http.ResponseWriter, r *http.Request) {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		sv, err := slack.NewSecretsVerifier(r.Header, signingSecret)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if _, err := sv.Write(body); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if err := sv.Ensure(); err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		eventsAPIEvent, err := slackevents.ParseEvent(body, slackevents.OptionNoVerifyToken())
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		if eventsAPIEvent.Type == slackevents.URLVerification {
			var r *slackevents.ChallengeResponse
			err := json.Unmarshal(body, &r)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			w.Header().Set("Content-Type", "text")
			w.Write([]byte(r.Challenge))
		}
		if eventsAPIEvent.Type == slackevents.CallbackEvent {
			innerEvent := eventsAPIEvent.InnerEvent
			switch ev := innerEvent.Data.(type) {
			case *slackevents.AppMentionEvent:
				if err := handleMention(ev); err != nil {
					log.Println("[ERROR]", err)
				}
			case *slackevents.MessageEvent:
				if err := handleMessage((innerEvent.Data).(*slackevents.MessageEvent)); err != nil {
					log.Println("[ERROR]", err)
				}
			default:
				log.Println("[INFO]", ev)
			}
		}
	})
	log.Println("[INFO] Server listening")
	if err := http.ListenAndServe(":3000", nil); err != nil {
		log.Fatalln("[FATAL]", err)
	}
}

func handleMention(message *slackevents.AppMentionEvent) error {
	_, _, err := api.PostMessage(message.Channel, slack.MsgOptionText(quote, false))
	if err != nil {
		err = fmt.Errorf("cannot respond to mention in %s: %w", message.Channel, err)
	}
	return err
}

func handleMessage(message *slackevents.MessageEvent) error {
	reply, err := kennabot.Consume(context.Background(), message.Text)
	if err == ErrOffendingKrishna && message.BotID == "" {
		api.PostMessage(message.Channel, slack.MsgOptionText(fmt.Sprintf("<@%s> %s", message.User, err.Error()), false))
	}

	if err != nil {
		return err
	}

	if reply != "" {
		_, _, err = api.PostMessage(message.Channel, slack.MsgOptionText(reply, false))
	}
	if err != nil {
		return fmt.Errorf("cannot comment on karma in %s: %w", message.Channel, err)
	}

	return nil
}
