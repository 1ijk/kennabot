module gitlab.com/1ijk/kennabot

go 1.15

require (
	github.com/go-redis/redis/v8 v8.5.0
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/slack-go/slack v0.8.1
	github.com/stretchr/testify v1.6.1
)
