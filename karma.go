package main

import (
	"errors"
	"math"
	"regexp"
	"strings"
)

const (
	None = 0
	More = 1
	Less = -1
)

var (
	NoKarma = Karma{
		"nobody",
		"naught",
		None,
	}

	MoreKarma = Karma{
		Val: More,
	}

	LessKarma = Karma{
		Val: Less,
	}

	WarnKarma = Karma{
		Why: ErrOffendingKrishna.Error(),
		Val: -math.MaxInt64,
	}

	expression = regexp.MustCompile(`^(.*?)(\+\+|--)(.*)?`)
	nonononono = regexp.MustCompile(`.* has (.*?) karma for .*`)

	ErrOffendingKrishna = errors.New("you have offended Krishna")
)

type Karma struct {
	Who string
	Why string
	Val int64
}

func Read(message string) Karma {
	if nonononono.MatchString(message) {
		return WarnKarma
	}

	message = strings.TrimSpace(message)

	if message == "++" {
		return MoreKarma
	}

	if message == "--" {
		return LessKarma
	}

	k := expression.FindStringSubmatch(message)

	if len(k) != 4 {
		return NoKarma
	}

	var points int64
	if k[2] == "++" {
		points = More
	}

	if k[2] == "--" {
		points = Less
	}

	who := k[1]
	who = strings.Replace(who, "@", "", 1)

	why := k[3]
	why = strings.Replace(why, "for", "", 1)
	why = strings.TrimSpace(why)

	return Karma{
		Who: who,
		Why: why,
		Val: points,
	}
}
