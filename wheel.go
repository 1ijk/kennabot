package main

var Hell = &Wheel{WarnKarma}

type Wheel struct {
	Eye Karma
}

func (w *Wheel) Turn(message string) Karma {
	k := Read(message)

	// you have offended Krishna
	if k == Hell.Eye {
		return Hell.Eye
	}

	// you have not done anything of note
	if k == NoKarma {
		return k
	}

	// the same person, assume the same reason
	if k.Why == "" && k.Who == w.Eye.Who {
		w.Eye.Val = k.Val
		return w.Eye
	}

	// a new person
	if k.Who != "" {
		w.Eye = k
	}

	// just karma points
	if k == MoreKarma || k == LessKarma {
		w.Eye.Val = k.Val
	}

	// same person but different reason
	if k.Why != "" && k.Why != w.Eye.Why {
		w.Eye.Why = k.Why
		w.Eye.Val = k.Val
	}

	return w.Eye
}
